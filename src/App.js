import React, { Component } from "react";
import NavBar from "./components/navbar/Navbar";
import Card from "./components/card/Card";

import GlobalStyles from "./styles/Global";

class App extends Component {
  state = {
    navbarOpen: false,
  };

  handleNavbar = () => {
    this.setState({ navbarOpen: !this.state.navbarOpen });
  };

  render() {
    return (
      <>
        <NavBar
          navbarState={this.state.navbarOpen}
          handleNavbar={this.handleNavbar}
        />
        <Card />
        <GlobalStyles />
      </>
    );
  }
}

export default App;
