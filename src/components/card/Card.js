import React, { Component } from "react";
import styled from "styled-components";
import { Howl } from "howler";

// https://firebasestorage.googleapis.com/v0/b/kevbot-test-app.appspot.com/o/Never%20Gonna%20Give%20You%20Up%20Original.mp3?alt=media&token=95db5c40-30a5-4e88-a20a-1fa2f3b7aeec

import CardFront from "./CardFront";

class Card extends Component {
  state = {
    sound: null,
    playing: false,
  };

  componentDidMount() {
    const sound = new Howl({
      src: [
        "https://firebasestorage.googleapis.com/v0/b/kevbot-test-app.appspot.com/o/Never%20Gonna%20Give%20You%20Up%20Original.mp3?alt=media&token=95db5c40-30a5-4e88-a20a-1fa2f3b7aeec",
      ],
      volume: 0.5,
    });

    this.setState({ sound });
  }

  componentWillUnmount() {
    this.state.sound.unload();
    this.setState({ sound: null, playing: false });
  }

  handlePlay = () => {
    if (this.state.playing) {
      this.state.sound.stop();
    } else {
      this.state.sound.play();
    }

    this.setState({ playing: !this.state.playing });
  };

  render() {
    return (
      <CardContainer>
        <CardFront handlePlay={this.handlePlay} playing={this.state.playing} />
      </CardContainer>
    );
  }
}

export default Card;

const CardContainer = styled.div`
  position: relative;
  max-width: 350px;
  width: 100%;
  height: 100%;
  margin: auto;
  margin-top: 400px;
`;

// const Card = styled.div`
//   margin-top: 100px;
//   background-color: transparent;
//   display: flex;
//   align-items: center;
//   justify-content: center;
//   width: 200px;
//   height: 260px;
//   position: relative;
//   transition: transform 1s;
//   transform-style: preserve-3d;
//   /* transform: rotateY(${(props) => (props.cardFlipped ? "180deg" : "0deg")}); */
// `;
