import React from "react";
import styled from "styled-components";

import variables from "../../styles/variables.scss";

const CardBack = (props) => {
  return (
    <CardFaceWrapper>
      <Title>Card Back</Title>
      <Paragraph>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates,
        veniam.
      </Paragraph>
    </CardFaceWrapper>
  );
};

export default CardBack;

const CardFaceWrapper = styled.div`
  position: absolute;
  background-color: blue;
  width: 90%;
  height: 90%;
  margin: 0 auto;
  backface-visibility: hidden;
  transform: rotateY(180deg);
`;

const Title = styled.h3`
  color: ${variables.whitecolor};
  font-size: 1.4rem;
`;

const Paragraph = styled.p`
  color: ${variables.whitecolor};
`;
