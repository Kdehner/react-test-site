import React from "react";
import styled from "styled-components";

import variables from "../../styles/variables.scss";

const CardFront = (props) => {
  return (
    <CardFace>
      <CardVisual />
      <CardInfos>
        {/* <CardInfoTitle>Card Title</CardInfoTitle>
        <CardInfoSub>Card Sub Text</CardInfoSub> */}
        <CardLinkButton onClick={props.handlePlay}>
          {props.playing ? "Stop" : "Listen"}
        </CardLinkButton>
      </CardInfos>
      <CardStatsWrapper>
        <CardStats>
          <CardStat>
            <CardStatTitle>Album Name</CardStatTitle>
            <CardStatSub>Track Name</CardStatSub>
          </CardStat>
          {/* <CardStat>
            <CardStatTitle>Card Stat</CardStatTitle>
            <CardStatSub>Card Stat Sub</CardStatSub>
          </CardStat>
          <CardStat>
            <CardStatTitle>Card Stat</CardStatTitle>
            <CardStatSub>Card Stat Sub</CardStatSub>
          </CardStat> */}
        </CardStats>
      </CardStatsWrapper>
    </CardFace>
  );
};

export default CardFront;

const CardFace = styled.div`
  float: left;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  width: 100%;
  height: 530px;

  background: ${variables.whitecolor};
  border-radius: ${variables.borderradius};
  z-index: 1;
  box-shadow: 0 0 5px rgba(black, 0.1);

  &:after {
    content: "";
    display: block;
    position: absolute;
    width: 100%;
    height: 100px;
    bottom: 0;
    box-shadow: 0 36px 64px -34px rgba(black, 1),
      0 16px 14px -14px rgba(black, 0.6), 0 22px 18px -18px rgba(black, 0.4),
      0 22px 38px -18px rgba(black, 1);
    transform: scaleX(0.7) scaleY(1.3) translateY(-15%);
    z-index: 1;
    opacity: 0.25;
  }
`;

const CardVisual = styled.div`
  height: ${variables.cardvisualheight};
  overflow: hidden;
  position: relative;
  border-top-left-radius: ${variables.borderradius};
  border-top-right-radius: ${variables.borderradius};

  &:before,
  &:after {
    display: block;
    content: "";
    width: 100%;
    height: 100%;
    position: absolute;
    z-index: 0;
    background: url("https://images.unsplash.com/flagged/photo-1587096472434-8b65b343980d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1275&q=80")
      no-repeat center center/cover;
  }
`;

const CardInfos = styled.div`
  position: absolute;
  z-index: 2;
  left: 0;
  right: 0;
  margin: auto;
  top: calc(${variables.cardvisualheight} - 100px);
  color: ${variables.whitecolor};
  text-align: center;
`;

const CardLinkButton = styled.button`
  position: absolute;
  left: 0;
  right: 0;
  top: calc(500% + 80px);
  margin: auto;
  width: 140px;
  height: 45px;
  font-family: "Roboto", sans-serif;
  font-size: 11px;
  text-transform: uppercase;
  letter-spacing: 2.5px;
  font-weight: 500;
  color: #000;
  background-color: #fff;
  border: none;
  border-radius: 45px;
  box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
  transition: all 0.3s ease 0s;
  cursor: pointer;
  outline: none;
`;

// const CardInfoTitle = styled.span`
//   display: block;
//   clear: both;
//   padding: 0.5rem 0;
//   padding-top: 0;
//   position: absolute;
//   width: 100%;
//   text-align: center;
//   font-size: 18px;
//   top: 8px;
//   font-weight: 800;
// `;

// const CardInfoSub = styled(CardInfoTitle)`
//   top: 32px;
//   font-size: 14px;
//   font-weight: 300;
// `;

const CardStatsWrapper = styled.div`
  background: ${variables.whitecolor};
  float: left;
  width: 100%;
  height: ${variables.cardstatsheight};
  border-bottom-left-radius: ${variables.borderradius};
  border-bottom-right-radius: ${variables.borderradius};
`;

const CardStats = styled.div`
  position: absolute;
  width: 100%;
  top: calc(70% + 4em);
  display: flex;
`;

const CardStat = styled.div`
  flex: 1;
  text-align: center;
`;

const CardStatTitle = styled.strong`
  display: block;
  float: left;
  clear: both;
  width: 100%;
  color: ${variables.cardstattitlecolor};
  font-size: 14px;
  font-weight: 500;
  letter-spacing: -0.2px;
`;

const CardStatSub = styled.span`
  font-size: 26px;
  color: ${variables.cardstatsubcolor};
  padding: 0.18em 0;
  display: inline-block;
`;

// const CardFaceWrapper = styled.div`
//   position: absolute;
//   background-color: red;
//   width: 90%;
//   height: 90%;
//   margin: 0 auto;
//   backface-visibility: hidden;
// `;

// const Title = styled.h3`
//   color: ${variables.whitecolor};
//   font-size: 1.4rem;
// `;

// const Paragraph = styled.p`
//   color: ${variables.whitecolor};
// `;
